import React from "react"
import AssemblyProcessPage from "./ui/AssemblyProcessPage";
import { Provider } from "react-redux";
import { assemblyProcessStore } from "./redux/AssemblyProcessStore";


const App: React.FC = () => {
  return (
    <Provider store={assemblyProcessStore}>
      <AssemblyProcessPage />
    </Provider>
  );
}

export default App;