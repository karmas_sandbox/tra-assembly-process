import { Component, Dispatch } from "react";
import React from "react";
import AssemblyProcessList from "./AssemblyProcessList";
import { Grid, Paper, RadioGroup, Radio, FormControlLabel, Box } from "@material-ui/core";
import RadioFilter from "./RadioFilter";
import { AssemblyStatus } from "../model/AssemblyStatus";
import { ReviewStatus } from "../model/ReviewStatus";
import { toArray } from "../utils/ToArray";
import { SubTitle } from "./Label";
import { ApplicationState, setReviewStatus, setAssemblyStatus } from "../redux/AssemblyProcessStore";
import { connect } from "react-redux";

interface Props{
    assemblyStatus?: AssemblyStatus, 
    reviewStatus?: ReviewStatus, 
    setAssemblyStatus?: (assemblyStatus: AssemblyStatus) => void, 
    setReviewStatus?: (reviewStatus: ReviewStatus) => void
}

export class AssemblyProcessSidePanel extends Component<Props> {
    render() {
        const {assemblyStatus, reviewStatus, setAssemblyStatus, setReviewStatus} = this.props

        return (
            <Box border={1} padding={2} borderColor="#D6DADB" style={{ marginRight: 48, marginTop: 24, background: '#F4F5F6' }}>
                <Grid container direction="column">
                    <SubTitle>Filter</SubTitle>
                    <RadioGroup>
                        <RadioFilter dataSet={toArray(AssemblyStatus)} currentValue={assemblyStatus} title="Assembly" onChange={(assemblyStatus) => {
                            if(setAssemblyStatus != undefined) {
                                setAssemblyStatus(assemblyStatus)
                            }}}/>
                        <RadioFilter dataSet={toArray(ReviewStatus)} currentValue={reviewStatus} title="Review" onChange={(reviewStatus) => {
                            if(setReviewStatus != undefined) {
                                setReviewStatus(reviewStatus)
                            }}}/>
                    </RadioGroup>
                </Grid>
            </Box>)
    }
}

function mapStateToProps(state: ApplicationState){
    const {assemblyStatus, reviewStatus} = state
    return {
        assemblyStatus: assemblyStatus,
        reviewStatus: reviewStatus 
    }
}
  
function mapDispatchToProps(dispatch: ((action: any) => void)){
    return {
        setAssemblyStatus: (assemblyStatus: AssemblyStatus) => dispatch(setAssemblyStatus(assemblyStatus)),
        setReviewStatus: (reviewStatus: ReviewStatus) => dispatch(setReviewStatus(reviewStatus))
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AssemblyProcessSidePanel);
  