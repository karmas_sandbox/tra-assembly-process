import React, { Component } from "react";
import { Grid, Chip, IconButton, InputBase, Paper, Box } from "@material-ui/core";
import { Title, InactiveLabel } from "./Label";
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import { toArray } from "../utils/ToArray";
import { Sorting } from "../model/Sorting";
import { ApplicationState, setSorting } from "../redux/AssemblyProcessStore";
import { connect } from "react-redux";

interface Props {
    sorting?: Sorting,
    setSortingOption?: (sorting: Sorting) => void
}

class DataContainer extends Component<Props> {
    handleSortingOptionChange(event: React.MouseEvent<HTMLElement>, newSorting: Sorting) {
        const { sorting, setSortingOption } = this.props
        if (sorting !== undefined && sorting !== newSorting && setSortingOption !== undefined) {
            setSortingOption(newSorting)
        }
    }

    render() {
        const { sorting } = this.props

        return (
            <Grid direction="column">
                <Grid container item direction="row" alignContent='center' style={{ marginTop: 16 }}>
                    <Grid item style={{ marginRight: 13 }}> <Title>Assembly process</Title></Grid>
                    <Grid item> <Chip size="small" label="100" style={{ marginTop: 8, backgroundColor: "#7F8487", color: "#FFFFFF", fontSize: 12, fontWeight: 700}}  /></Grid>
                    <Grid item xs></Grid>
                    <Grid item style={{ paddingTop: 8, marginRight: 13 }}><InactiveLabel>Show</InactiveLabel></Grid>
                    <Grid item style={{ marginRight: 12 }}> <ToggleButtonGroup size="small" exclusive value={sorting} onChange={this.handleSortingOptionChange.bind(this)}>
                        {this.generateSortingOptions(toArray(Sorting))}
                    </ToggleButtonGroup></Grid>
                    <Grid item><Box border={1} borderRadius={20} borderColor="#D6DADB" style={{width: 400, alignContent: 'center', justifyContent: 'center'}}>
                        <IconButton aria-label="Search">
                            <img src={require("../res/searchIcon.svg")} />
                        </IconButton>
                        <InputBase style={{width: 320}}
                            placeholder="Search by assembly name"
                            inputProps={{ 'aria-label': 'Search by assembly name' }}
                        /></Box></Grid>
                </Grid>
                <Grid item style={{marginTop: 16}}>{this.props.children}</Grid>
            </Grid>)
    }

    generateSortingOptions(dataSet: Array<any>) {
        return dataSet.map(entry => <ToggleButton key={entry} value={entry}>
            {entry}
        </ToggleButton>)
    }
}

export function mapStateToProps(state: ApplicationState){
    const {sorting} = state
    return {
        sorting: sorting
    }
}
  
export function mapDispatchToProps(dispatch: ((action: any) => void)){
    
    return {
        setSortingOption: (sorting: Sorting) => {
            console.error(sorting) 
            dispatch(setSorting(sorting))
        } 
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DataContainer);
  