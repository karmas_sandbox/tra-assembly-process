import { Component } from "react";
import React from "react";
import { InactiveLabel } from "./Label";
import { RadioGroup, FormControlLabel, Radio, Grid } from "@material-ui/core";
import { connect } from "http2";


interface Props {
    title: String,
    currentValue: any,
    dataSet: Array<any>

    onChange: (value: any) => void
}

export default class RadioFilter extends Component<Props> {
    render() {
        const {dataSet, currentValue, title, onChange} = this.props

        return (
        <Grid container direction='column'>
            <InactiveLabel>{title}</InactiveLabel>
            <RadioGroup value={currentValue} onChange={this.setValue.bind(this)}>
                {this.generateOptions(dataSet)}
            </RadioGroup>
        </Grid>)
    }

    generateOptions(dataSet: Array<any>){
        return dataSet.map(element => <Grid item><FormControlLabel
                value={element}
                control={<Radio color="primary" icon={(<img src={require("../res/filterOption.svg")}/>)} checkedIcon={(<img src={require("../res/filterOptionChecked.svg")}/>)}/>}
                label={element}
                labelPlacement="end"
            /></Grid> );
    }

    setValue(event: React.ChangeEvent<HTMLElement>, newValue: any) {
    
        const {currentValue, onChange} = this.props

        if(newValue != currentValue) { // TODO Is it usefull ?
            this.setState({value: newValue})
            onChange(newValue)
        }
    }

    
}