import React, { Component } from "react";
import { List } from "react-virtualized"
import { Grid, FormLabel, Paper, Box, Avatar, ButtonBase, Typography, Divider, Chip, Container, Card } from "@material-ui/core";
import { Label, SubTitle, Title, InactiveLabel } from "./Label";
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { Item } from "../model/Item";
import { getBadge } from "./ReviewBadges";

interface Props {
    item: Item
}

export default class AssemblyProcessCell extends Component<Props> {

    render() {
        const {item} = this.props
        return (
            <Paper style={{
                margin: 4,
                padding: 4,
                height: 132,
                marginRight: 8
            }}>

                <Grid direction="row" item container spacing={2} xs>

                    <Grid item style={{ width: 200 }}>
                        <img alt="Component name" src={require("../res/item-placeholder.svg")} style={{ position: 'absolute' }} />
                        {getBadge(item.assemblyStatus)}
                    </Grid>

                    <Grid direction="column" justify="center" item container spacing={0} xs sm>
                        <SubTitle>{item.title}</SubTitle>

                        <Grid direction="row" item container spacing={0}>
                            <InactiveLabel>Review</InactiveLabel>
                            <Grid item xs><Box marginLeft={1} marginRight={1} height={0.95} borderBottom={1} color='#BDC0C1' /></Grid>
                            <Label>{item.reviewStatus}</Label>
                        </Grid>
                        
                        <Grid direction="row" item container spacing={0}>
                            <InactiveLabel>Lust Updates</InactiveLabel>
                            <Grid item xs><Box marginLeft={1} marginRight={1} height={0.95} borderBottom={1} color='#BDC0C1' /></Grid>
                            <Label>{item.updated}</Label>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>)
    }


}