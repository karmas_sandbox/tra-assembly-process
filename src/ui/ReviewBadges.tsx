
import { AssemblyStatus } from "../model/AssemblyStatus"
import React, { Component } from "react"
import { Box } from "@material-ui/core"
import { Label, BadgeText } from "./Label"

export class InReviewBadge extends Component {
    render() {
        return (<Box height={20} width={63} border={1} borderColor='#2B000000' borderRadius={3} style={{ backgroundColor: "#FFC800" }}><BadgeText>{AssemblyStatus.IN_REVIEW}</BadgeText></Box>)
    }
}

export class ReviewFinishedBadge extends Component {
    render() {
        return (<Box height={20} width={63} border={1} borderColor='#AFB3B6' borderRadius={3}><BadgeText>finished</BadgeText></Box>)
    }
}

export function getBadge(reviewStatus: String) {
    switch (reviewStatus) {
        case "IN_REVIEW": return (<InReviewBadge />)
        case "REVIEW_FINISHED": return (<ReviewFinishedBadge />)
        default: return null
    }
}