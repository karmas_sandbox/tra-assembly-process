import React, {Component} from 'react';
import AssemblyProcessCell from './AssemblyProcessCell';
import { FixedSizeList, ListChildComponentProps } from 'react-window'
import { Item } from '../model/Item';
import { Title } from './Label';
import { AutoSizer } from 'react-virtualized';
import { connect } from 'react-redux';
import { getStaticData } from '../res/getStaticData';
import { stat } from 'fs';
import { ApplicationState } from '../redux/AssemblyProcessStore';

interface Props {
  data?: Item[]
}

class AssemblyProcessList extends Component<Props> {
  render() {
    const data: Item[] = this.props.data !== undefined ? this.props.data : []
    return (
      <FixedSizeList width="100v" height={800} itemSize={144} itemCount={data.length}>
        { props =>  getRow({...props, data}) }
      </FixedSizeList>);
  }
}

function getRow(props: ListChildComponentProps) {
  const { index, style, data } = props;
  return (<div style={style}><AssemblyProcessCell item={data[index]}/></div>);
}

function mapStateToProps(state: ApplicationState){
  return {
    data: getStaticData(state.assemblyStatus, state.reviewStatus, state.sorting),
  }
}

export default connect(mapStateToProps, null)(AssemblyProcessList);
