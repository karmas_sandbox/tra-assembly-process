import React, { Component } from "react";
import color from "@material-ui/core/colors/yellow";
import { Typography } from "@material-ui/core";

export class Label extends Component {
    render() {
        return (<Typography style={styles.label}>{this.props.children}</Typography>)
    }
}

export class InactiveLabel extends Component {
    render() {
        return (<Typography style={styles.inactiveLabel}>{this.props.children}</Typography>)
    }
}

export class SubTitle extends Component {
    render() {
        return (<Typography style={styles.subTitle}>{this.props.children}</Typography>)
    }
}

export class Title extends Component {
    render() {
        return (<Typography style={styles.title}>{this.props.children}</Typography>)
    }
}

export class BadgeText extends Component {
    render() {
        return (<Typography align='center' style={styles.badgeText}>{this.props.children}</Typography>)
    }
}

const styles = {
    label: {
        margin: 0, 
        fontSize: 14,
        color: "black",
    },
    inactiveLabel: {
        margin: 0, 
        fontSize: 14,
        color: "grey",
    },
    badgeText: {
        margin: 3, // TODO Can't center this m-fucker in sane way
        fontSize: 11,
        color: "black",
        fontWeight: 700,
    },
    subTitle: {
        margin: 0, 
        fontSize: 18,
        lineHeight: 1.5,
        color: "black",
        fontWeight: 700
    },
    title: {
        margin: 0, 
        fontSize: 24,
        color: "black",
        fontWeight: 700
    },
}
