import { Component } from "react";
import React from "react";
import AssemblyProcessList from "./AssemblyProcessList";
import { Grid } from "@material-ui/core";
import AssemblyProcessHeader from "./AssemblyProcessHeader";
import AssemblyProcessSidePanel from "./AssemblyProcessSidePanel";
import DataContainer from "./OutputPanel";

export default class AssemblyProcessPage extends Component {
    render() {
        return (
            <Grid direction="column" container>
                <Grid item><AssemblyProcessHeader/></Grid>
                <Grid item container direction="row" xs>
                    <Grid item style={{marginLeft: 80}}><AssemblyProcessSidePanel/></Grid>
                    <Grid item xs style={{marginRight: 176}}>
                        <DataContainer >
                            <AssemblyProcessList/>
                        </DataContainer>
                    </Grid>
                </Grid>
            </Grid>)
    }
}