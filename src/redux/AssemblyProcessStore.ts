import { createStore, Store, Action, combineReducers } from 'redux'
import { Sorting } from '../model/Sorting';
import { AssemblyStatus } from '../model/AssemblyStatus';
import { ReviewStatus } from '../model/ReviewStatus';
import { Reducer } from 'react';
import { stat } from 'fs';

const SET_ASSEMBLY_STATUS = "SET_ASSEMBLY_STATUS"
const SET_REVIEW_STATUS = "SET_REVIEW_STATUS"
const SET_SORTING = "SET_SORTING"

export interface ApplicationState {
    assemblyStatus: AssemblyStatus,
    reviewStatus: ReviewStatus,
    sorting: Sorting
}

const initialState: ApplicationState = {
    assemblyStatus: AssemblyStatus.ANY,
    reviewStatus: ReviewStatus.ANY,
    sorting: Sorting.LATEST_FIRST
}

function filtersReducer(state = initialState, action: SetAssemblyStatus | SetReviewStatus | SetSorting): ApplicationState{
    if(state === undefined) {
        state = initialState
    }

    switch (action.type) {
        case SET_ASSEMBLY_STATUS: return { ...state, ...action.payload }
        case SET_REVIEW_STATUS: return { ...state, ...action.payload }
        case SET_SORTING: return { ...state, ...action.payload }
        default: return state
    }
}

export interface SetAssemblyStatus {
    type: String
    payload: {assemblyStatus: AssemblyStatus}
}

export function setAssemblyStatus(assemblyStatus: AssemblyStatus) {
    return {
        type: SET_ASSEMBLY_STATUS,
        payload: { assemblyStatus },
    }
}

export interface SetReviewStatus {
    type: String
    payload: { reviewStatus: ReviewStatus }
}

export function setReviewStatus(reviewStatus: ReviewStatus) {
    return {
        type: SET_REVIEW_STATUS,
        payload:   { reviewStatus } ,
    }
}

export interface SetSorting {
    type: String
    payload: { sorting: Sorting } 
}

export function setSorting(sorting: Sorting) {
    return {
        type: SET_SORTING,
        payload: { sorting },
    }
}

export const assemblyProcessStore = createStore(filtersReducer, initialState)

