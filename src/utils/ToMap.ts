export function toArray(givenEnum: any) {
    return Object.keys(givenEnum)
        .filter(value => isNaN(Number(value)) === false)
        .map(key => new Map([key, givenEnum[key]]));
}