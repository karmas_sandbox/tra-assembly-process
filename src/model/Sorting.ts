export enum Sorting {
    LATEST_FIRST = "Latest first",
    OLD_FIRST = "Old first"
}