import { AssemblyStatus } from "./AssemblyStatus";
import { ReviewStatus } from "./ReviewStatus";

export interface Item { //move to model
    "_id": string,
    "img": string,
    "age": Number,
    "assemblyStatus": AssemblyStatus,
    "reviewStatus": ReviewStatus,
    "title": string,
    "updated": string
  }