export enum AssemblyStatus {
    ANY = "Any",
    IN_REVIEW = "In review",
    REVIEW_FINISHED = "Review finish",
}

export function assemblyStatusFromKey(key: String) : AssemblyStatus {
    switch(key) {
        case "ANY": return AssemblyStatus.ANY
        case "IN_REVIEW": return AssemblyStatus.IN_REVIEW
        default: return AssemblyStatus.REVIEW_FINISHED
    }
}
