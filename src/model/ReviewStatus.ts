export enum ReviewStatus {
    ANY = "Any",
    DRAFT = "Draft",
    SOLVED = "Solved",
    SIMULATION_REQUESTED = "Simulation requested",
    SIMOLATION_POSITIVE = "Simulation positive",
    SIMULATION_NEGATIVE = "Simulation negative",
}

// TODO TS Enums are wierd

export function reviewStatusFromKey(key: String): ReviewStatus {
    switch(key) {
        case "DRAFT" : return ReviewStatus.DRAFT 
        case "SOLVED" : return ReviewStatus.SOLVED 
        case "SIMULATION_REQUESTED" : return ReviewStatus.SIMULATION_REQUESTED 
        case "SIMULATION_POSITIVE" : return ReviewStatus.SIMOLATION_POSITIVE 
        case "SIMULATION_NEGATIVE" : return ReviewStatus.SIMULATION_NEGATIVE 
        default: return ReviewStatus.ANY
    }
}