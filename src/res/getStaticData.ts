import { AssemblyStatus, assemblyStatusFromKey } from "../model/AssemblyStatus"
import { ReviewStatus, reviewStatusFromKey } from "../model/ReviewStatus"
import { Sorting } from "../model/Sorting"
import { Item } from "../model/Item"

const allItems: Item[] = require("./static_data.json")

export function getStaticData(assemblyStatus: AssemblyStatus, reviewStatus: ReviewStatus, sorting: Sorting) {
    return allItems
        .filter(item => (assemblyStatus === AssemblyStatus.ANY || assemblyStatus === assemblyStatusFromKey(item.assemblyStatus))
            && (reviewStatus === ReviewStatus.ANY || reviewStatus === reviewStatusFromKey(item.reviewStatus)))
        .sort((firstItem, secondItem) => (new Date(secondItem.updated.replace(" ", "")).getTime() - new Date(firstItem.updated.replace(" ", "")).getTime()) * (sorting === Sorting.LATEST_FIRST ? 1 : -1))
}